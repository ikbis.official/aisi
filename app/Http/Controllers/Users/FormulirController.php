<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Models\Formkes;
use App\Models\Formulir;
use Illuminate\Http\Request;

class FormulirController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $formulir = Formulir::paginate(10);
        // $formkes = Formkes::all();
        // dd($inputKesehatan);
        return view('timtest.dataformulir', ['formulirs' => $formulir]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.registrasi');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validasi form
        $this->validate($request, [
            'name' => 'required',
            'image' => 'required|mimes:jpg, jpeg, png',
            'alamat' => 'required',
            'kecamatan' => 'required',
            'kota' => 'required',
            'kelurahan' => 'required',
            'kodepos' => 'required',
            'provinsi' => 'required',
            'nik' => 'required|numeric',
            'email' => 'required|email',
            'hp' => 'required',
            'tempatlahir' => 'required',
            'tanggallahir' => 'required',
            'agama' => 'required',
            'jk' => 'required',
            'status_perkawinan' => 'required',
            'nameayah' => 'required',
            'alamatOrTua' => 'required',
            'KodePosOrTua' => 'required',
            'ProvinsiOrTua' => 'required',
            'kota' => 'required',
            'hpOrTua' => 'required',
            'TelpOrTua' => 'required',
            'pekerjaan' => 'required',
            'NamaInstansi' => 'required',
            'penghasilan' => 'required',
            'penghasilan' => 'required',
            'namaibu' => 'required',
            'PekerjaanIbu' => 'required',
            'mahasiswa' => 'required',
            'smapt' => 'required',
            'jurusan' => 'required',
            'alamatSekolahPt' => 'required',
            'noSttbIjazah' => 'required',
            'skhu' => 'required',
            'Tahun' => 'required',
            'prodi' => 'required',
            'kelas' => 'required',


        ]);

        $form_regist = new \App\Models\Formulir;

        $img = $request->image;
        $img_name = time() . '-' . $img->getClientOriginalName();

        $form_regist->name = $request['name'];
        $form_regist->image = $img_name;
        $form_regist->alamat = $request['alamat'];
        $form_regist->kecamatan = $request['kecamatan'];
        $form_regist->kota = $request['kota'];
        $form_regist->kelurahan = $request['kelurahan'];
        $form_regist->kode_pos = $request['kodepos'];
        $form_regist->provinsi = $request['provinsi'];
        $form_regist->nik = $request['nik'];
        $form_regist->email = $request['email'];
        $form_regist->hp = $request['hp'];
        $form_regist->tempat_lahir = $request['tempatlahir'];
        $form_regist->tanggal_lahir = $request['tanggallahir'];
        $form_regist->agama = $request['agama'];
        $form_regist->jenis_kelamin = $request['jk'];
        $form_regist->status_perkawinan = $request['status_perkawinan'];
        $form_regist->nama_ayah = $request['nameayah'];
        $form_regist->alamat_ayah = $request['alamatOrTua'];
        $form_regist->kode_pos_ayah = $request['KodePosOrTua'];
        $form_regist->provinsi_ayah = $request['ProvinsiOrTua'];
        $form_regist->kota_kab_ayah = $request['kota'];
        $form_regist->hp_orangtua = $request['hpOrTua'];
        $form_regist->telp_rumah = $request['TelpOrTua'];
        $form_regist->pekerjaan = $request['pekerjaan'];
        $form_regist->nama_instansi = $request['NamaInstansi'];
        $form_regist->penghasilan_ortu = $request['penghasilan'];
        $form_regist->nama_ibu = $request['namaibu'];
        $form_regist->pekerjaan_ibu = $request['PekerjaanIbu'];
        $form_regist->sebagai_mahasiswa = $request['mahasiswa'];
        $form_regist->sma_pt = $request['smapt'];
        $form_regist->jurusan = $request['jurusan'];
        $form_regist->alamat_sekolah = $request['alamatSekolahPt'];
        $form_regist->no_sttb = $request['noSttbIjazah'];
        $form_regist->no_skhu = $request['skhu'];
        $form_regist->tahun_lulus = $request['Tahun'];
        $form_regist->prodi = $request['prodi'];
        $form_regist->kelas = $request['kelas'];

        // dd($form_regist);
        $img->move('img/uploads/camaba', $img_name);
        $form_regist->save();
        return redirect()->route('mahasiswa.registrasi')->with('massage', 'Form Registrasi Mahasiswa/i baru telah berhasil di tambahkan.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Formulir  $formulir
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $formulir = Formulir::findOrFail($id);
        return view('users.show', ['formulir' => $formulir]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Formulir  $formulir
     * @return \Illuminate\Http\Response
     */
    public function edit(Formulir $formulir)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Formulir  $formulir
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Formulir $formulir)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Formulir  $formulir
     * @return \Illuminate\Http\Response
     */
    public function destroy(Formulir $formulir)
    {
        //
    }
}
