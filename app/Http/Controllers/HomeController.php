<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (auth()->user()->hasRole('admin')) {
            return view('admin.index');
        } else if (auth()->user()->hasRole('timtest')) {
            return view('timtest.index');
        } else if (auth()->user()->hasRole('user')) {
            return view('home');
        }
        // return redirect()->route('home'); Default Route
    }
}
