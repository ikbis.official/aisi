<?php

namespace App\Http\Controllers\timtest;

use App\Http\Controllers\Controller;
use App\Models\Timtest;
use Illuminate\Http\Request;

class TimTestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('timtest.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Timtest  $timtest
     * @return \Illuminate\Http\Response
     */
    public function show(Timtest $timtest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Timtest  $timtest
     * @return \Illuminate\Http\Response
     */
    public function edit(Timtest $timtest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Timtest  $timtest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Timtest $timtest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Timtest  $timtest
     * @return \Illuminate\Http\Response
     */
    public function destroy(Timtest $timtest)
    {
        //
    }
}
