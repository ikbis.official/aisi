<?php

namespace App\Http\Controllers\timtest;

use App\Http\Controllers\Controller;
// Origin models
use App\Models\Formkes;
// use Models
use App\Models\Formulir;
use Illuminate\Http\Request;

class FormKesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        // $formkes = Formulir::find($id)->formkes;
        // $formkes = Formkes::find($id)->formulir;

        // $formkes = Formkes::find($id);
        $formulir = Formulir::find($id);
        // dd($formkes);
        return view('timtest.formkes', compact('formulir', 'formkes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $formkes = Formkes::insert([
            'name' => $request->name,
            'usia' => $request->usia,
            'jenis_kelamin' => $request->jenis_kelamin,
            'prodi' => $request->prodi,
            'tinggi_bb' => $request->tinggi_badan,
            'tensi' => $request->tensi,
            'rr' => $request->rr,
            'nadi' => $request->nadi,
            'virus' => $request->virus,
            'butawarna' => $request->butawarna,
            'pendengaran' => $request->pendengaran,
            'jantung' => $request->jantung,
            'paru' => $request->paru,
            'urine' => $request->urin,
            'catatan_khusus' => $request->catatan,
            'kesimpulan' => $request->kesimpulan,
            'formulir_id' => $request->formkes_id
        ]);

        return redirect()->route('dashboard.registrasi')->with('massage', 'Formulir Kesehatan Berhasil Di Input');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Formkes  $formkes
     * @return \Illuminate\Http\Response
     */
    public function show(Formkes $formkes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Formkes  $formkes
     * @return \Illuminate\Http\Response
     */
    public function edit(Formkes $formkes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Formkes  $formkes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Formkes $formkes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Formkes  $formkes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Formkes $formkes)
    {
        //
    }
}
