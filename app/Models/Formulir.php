<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Formkes;

class Formulir extends Model
{
    use HasFactory;

    protected $table = 'fregistrasi';
    protected $guarded = [];

    public function formkes()
    {
        return $this->hasOne(Formkes::class, 'formulir_id');
        // OR return $this->hasOne('App\Formkes');
    }
}
