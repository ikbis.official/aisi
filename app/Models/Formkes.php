<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Formulir;

class Formkes extends Model
{
    use HasFactory;

    protected $table = 'fteskes';
    protected $guarded = [];

    public function formulir()
    {
        return $this->belongsTo(Formulir::class);
        // OR return $this->belongsTo('App\Formulir');
    }
}
