<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'name' => 'admin pmb',
            'email' => 'adminpmb@gmail.test',
            'password' => bcrypt(123456)
        ]);

        $admin->assignRole('admin');

        $timtest = User::create([
            'name' => 'timtest pmb',
            'email' => 'timtestpmb@gmail.test',
            'password' => bcrypt(123456)
        ]);

        $timtest->assignRole('timtest');

        $user = User::create([
            'name' => 'user',
            'email' => 'user@gmail.test',
            'password' => bcrypt(123456)
        ]);

        $user->assignRole('user');
    }
}
