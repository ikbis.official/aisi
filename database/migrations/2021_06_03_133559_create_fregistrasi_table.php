<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFregistrasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fregistrasi', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('alamat');
            $table->string('kecamatan');
            $table->string('kota');
            $table->string('kelurahan');
            $table->bigInteger('kode_pos');
            $table->bigInteger('nik');
            $table->string('provinsi');
            $table->string('email');
            $table->bigInteger('hp');
            $table->string('tempat_lahir');
            $table->date('tanggal_lahir');
            $table->string('agama');
            $table->string('jenis_kelamin');
            $table->string('status_perkawinan');
            $table->string('nama_ayah');
            $table->string('alamat_ayah');
            $table->string('kota_kab_ayah');
            $table->bigInteger('telp_rumah');
            $table->bigInteger('kode_pos_ayah');
            $table->string('provinsi_ayah');
            $table->bigInteger('hp_orangtua');
            $table->string('pekerjaan');
            $table->string('nama_instansi');
            $table->string('penghasilan_ortu');
            $table->string('nama_ibu');
            $table->string('pekerjaan_ibu');
            $table->string('sebagai_mahasiswa');
            $table->string('sma_pt');
            $table->string('jurusan');
            $table->string('alamat_sekolah');
            $table->string('no_sttb');
            $table->string('no_skhu');
            $table->date('tahun_lulus');
            $table->string('prodi');
            $table->string('kelas');
            $table->string('image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fregistrasi');
    }
}
