<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFteskesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fteskes', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('usia');
            $table->string('jenis_kelamin');
            $table->string('prodi');
            $table->string('tinggi_bb');
            $table->string('tensi');
            $table->string('nadi');
            $table->string('rr');
            $table->string('virus');
            $table->string('butawarna');
            $table->string('pendengaran');
            $table->string('jantung');
            $table->string('paru');
            $table->string('urine');
            $table->text('catatan_khusus');
            $table->text('kesimpulan');
            $table->unsignedBigInteger('formulir_id');
            $table->timestamps();
            $table->foreign('formulir_id')->references('id')->on('fregistrasi')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fteskes');
    }
}
