<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
// Route::middleware('role:admin')->get('/dashboard', function () {
//     return 'dashboard';
// })->name('dashboard');
// Route::middleware('role:timtest')->get('/dashboard/tim', function () {
//     return 'dashboard tim test';
// })->name('dashboard.tim');

// Route::group(['middleware' => ['role:admin']], function () {
//     Route::get('/admin', [App\Http\Controllers\admin\AdminController::class, 'index'])->name('dashboard');
// });

Route::group(['prefix' => 'dashboard', 'middleware' => ['auth', 'role:admin']], function () {
    // Route Home Dashboard User Admin
    Route::get('/admin', [App\Http\Controllers\admin\AdminController::class, 'index'])->name('dashboard');
});

Route::group(['prefix' => 'dashboard', 'middleware' => ['auth', 'role:timtest|admin']], function () {
    // View Dashboard
    Route::get('/timtest', [App\Http\Controllers\timtest\TimTestController::class, 'index'])->name('dashboard.tim');
    // Data Binding formulir
    Route::get('/formulir', [App\Http\Controllers\Users\FormulirController::class, 'index'])->name('dashboard.registrasi');
    // Return View Formulir and Post Formulir
    Route::get('/formkes/{id}', [App\Http\Controllers\timtest\FormKesController::class, 'create'])->name('formkes');
    Route::post('/formkes', [App\Http\Controllers\timtest\FormKesController::class, 'store'])->name('formkes.store');
});

Route::group(['middleware' => ['auth', 'role:user|admin']], function () {
    Route::get('/home', [App\Http\Controllers\Users\UsersController::class, 'index'])->name('home');
    // Route::get('/home', [App\Http\Controllers\Users\UsersController::class, 'index'])->name('home')->withoutMiddleware('auth');
    // Route Formulir Mahasiswa baru
    Route::get('/formulir', [App\Http\Controllers\Users\FormulirController::class, 'create'])->name('mahasiswa.registrasi');
    Route::post('/formulir', [App\Http\Controllers\Users\FormulirController::class, 'store'])->name('mahasiswa.registrasi');
});
