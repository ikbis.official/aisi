<x-app-layout title="formulir kesehatan">
    <section class="content">
        <div class="card shadow">
            <div class="card-header">
                Formulir Kesehatan
                <a href="{{ route('dashboard.registrasi') }}" class="btn btn-primary btn-sm float-right"><i
                        class="fas fa-chevron-left"></i> Back</a>
            </div>
            <div class="card-body">
                <form action="{{ route('formkes.store') }}" method="post">
                    @csrf
                    <div class="row">
                        <input type="hidden" name="formkes_id" value="{{ $formulir->id }}">
                        <div class="col">
                            <label class="form-label">Full Name</label>
                            <input type="text" class="form-control" name="name" value="{{ $formulir->name }}"
                                aria-label="Full Name" readonly>
                        </div>
                        <div class="col">
                            <label class="form-label">Jenis Kelamin</label>
                            <input type="text" class="form-control" name="jenis_kelamin" aria-label="Jenis Kelamin"
                                value="{{ $formulir->jenis_kelamin }}" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label class="form-label">Prodi</label>
                            <input type="text" class="form-control" name="prodi" value="{{ $formulir->prodi }}"
                                aria-label="Prodi" readonly>
                        </div>
                        <div class="col">
                            <label class="form-label">Usia</label>
                            <input type="text" class="form-control @error('usia') is-invalid @enderror" name="usia"
                                aria-label="Usia" placeholder="Usia" value="{{ old('usia') }}">
                            @error('usia')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label class="form-label">Tinggi Badan</label>
                            <input type="number" class="form-control @error('tinggi_badan') is-invalid @enderror"
                                name="tinggi_badan" aria-label="Tinggi badan" placeholder="Tinggi Badan"
                                value="{{ old('tinggi_badan') }}">
                            @error('tinggi_badan')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col">
                            <label class="form-label">tensi</label>
                            <input type="number" class="form-control @error('tensi') is-invalid @enderror" name="tensi"
                                aria-label="tensi" placeholder="Tensi" value="{{ old('tensi') }}">
                            @error('tensi')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label class="form-label">RR</label>
                            <input type="text" class="form-control @error('rr') is-invalid @enderror" name="rr"
                                aria-label="rr" placeholder="RR" value="{{ old('rr') }}">
                            @error('rr')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col">
                            <label class="form-label">Virus</label>
                            <input type="text" class="form-control @error('virus') is-invalid @enderror" name="virus"
                                aria-label="virus" placeholder="Virus" value="{{ old('virus') }}">
                            @error('virus')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label class="form-label">Butawarna</label>
                            <input type="text" class="form-control @error('butawarna') is-invalid @enderror"
                                name="butawarna" aria-label="butawarna" placeholder="Butawarna"
                                value="{{ old('butawarna') }}">
                            @error('butawarna')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col">
                            <label class="form-label">Pendengaran</label>
                            <input type="text" class="form-control @error('pendengaran') is-invalid @enderror"
                                name="pendengaran" aria-label="pendengaran" placeholder="Pendengaran"
                                value="{{ old('pendengaran') }}">
                            @error('pendengaran')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label class="form-label">Jantung</label>
                            <input type="text" class="form-control @error('jantung') is-invalid @enderror"
                                name="jantung" aria-label="jantung" placeholder="Jantung" value="{{ old('jantung') }}">
                            @error('jantung')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col">
                            <label class="form-label">Paru</label>
                            <input type="text" class="form-control @error('paru') is-invalid @enderror" name="paru"
                                aria-label="paru" placeholder="Paru" value="{{ old('paru') }}">
                            @error('paru')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label class="form-label">Nadi</label>
                            <input type="text" class="form-control @error('nadi') is-invalid @enderror" name="nadi"
                                aria-label="nadi" placeholder="nadi" value="{{ old('nadi') }}">
                            @error('nadi')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col">
                            <label class="form-label">Test Urin</label>
                            <input type="text" class="form-control @error('urin') is-invalid @enderror" name="urin"
                                aria-label="urin" placeholder="urin" value="{{ old('urin') }}">
                            @error('urin')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label for="catatan">Catatan Khusus</label>
                            <textarea class="form-control @error('catatan') is-invalid @enderror" name="catatan"
                                placeholder="Catatan Khusus" id="catatan" value="{{ old('catatan') }}"></textarea>
                            @error('catatan')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col">
                            <label for="kesimpulan">Kesimpulan</label>
                            <textarea class="form-control" name="kesimpulan" placeholder="Kesimpulan" id="kesimpulan"
                                value="{{ old('kesimpulan') }}"></textarea>
                            @error('kesimpulan')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <button type="submit" class="btn btn-success mt-2">Submit</button>
                </form>
            </div>
        </div>

    </section>
    <!-- /.content -->
</x-app-layout>