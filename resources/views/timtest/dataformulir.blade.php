<x-app-layout title="data formulir baru">
    <section class="content">
        {{-- alert --}}
        @if (session('massage'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Success!</strong> {{ session('massage') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        <table class="table table-striped table-dark">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Name</th>
                    <th scope="col">Jenis Kelamin</th>
                    <th scope="col">tempat lahir</th>
                    <th scope="col">tanggal lahir</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($formulirs as $key=>$formulir)
                <tr>
                    <th>{{ $key + 1 }}</th>
                    <td>{{ $formulir->name }}</td>
                    <td>{{ $formulir->jenis_kelamin }}</td>
                    <td>{{ $formulir->tempat_lahir }}</td>
                    <td>{{ $formulir->tanggal_lahir }}</td>
                    <td>
                        <a href="{{ route('formkes', [$formulir->id]) }}" class="btn btn-warning btn-sm"
                            data-toggle="tooltip" data-placement="top" title="Formulir"><i
                                class="fas fa-file-alt"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $formulirs->links() }}
    </section>
    <!-- /.content -->
</x-app-layout>