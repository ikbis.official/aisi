<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $title }}</title>
</head>

<body>

    <x-navbar />
    @isset($header)
    <div style="color:red, background-color:rgba(0, 0, 0, 5)">
        {{ $header }}
    </div>
    @endisset

    {{ $slot }}

    @stack('scripts')
</body>

</html>