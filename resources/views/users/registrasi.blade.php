<x-app-layout title="Form Registrasi">
    <section class="content">
        {{-- alert --}}
        @if (session('massage'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Success!</strong> {{ session('massage') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif


        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Form Registrasi</h3>
            </div>
            <div class="card-body">
                <form action="{{ route('mahasiswa.registrasi') }}" method="POST" enctype="multipart/form-data">
                    @csrf

                    <h5>Data Pribadi</h5>
                    <div class="mb-3">
                        <label for="FullName" class="form-label">Nama
                            Lengkap</label>
                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror"
                            id="FullName" value="{{ old('name') }}" placeholder="Nama Lengkap">

                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <label for="customFile" class="form-label">Foto</label>
                    <div class="custom-file">
                        <input type="file" name="image" class="custom-file-input @error('image') is-invalid @enderror"
                            id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>

                        @error('image')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="row">
                        <div class="col">
                            <label for="alamat" class="form-label">Alamat</label>
                            <input type="text" name="alamat" class="form-control @error('alamat') is-invalid @enderror"
                                placeholder="Alamat" value="{{ old('alamat') }}">
                            @error('alamat')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col">
                            <label for="kecamatan" class="form-label">Kecamatan</label>
                            <input type="text" name="kecamatan"
                                class="form-control @error('kecamatan') is-invalid @enderror" placeholder="Kecamatan"
                                value="{{ old('kecamatan') }}">
                            @error('kecamatan')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <label for="kota" class="form-label">Kota / Kab</label>
                            <input type="text" name="kota" class="form-control @error('kota') is-invalid @enderror"
                                placeholder="Kota / Kab" value="{{ old('kota') }}">
                            @error('kota')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col">
                            <label for="Keluarahan" class="form-label">Keluarahan</label>
                            <input type="text" name="kelurahan"
                                class="form-control @error('kelurahan') is-invalid @enderror" placeholder="Kelurahan"
                                value="{{ old('kelurahan') }}">
                            @error('kelurahan')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <label for="kodepos" class="form-label">Kode Pos</label>
                            <input type="text" name="kodepos"
                                class="form-control @error('kodepos') is-invalid @enderror" placeholder="Kode Pos"
                                value="{{ old('kodepos') }}">
                            @error('kodepos')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col">
                            <label for="provinsi" class="form-label">Provinsi</label>
                            <input type="text" name="provinsi"
                                class="form-control @error('provinsi') is-invalid @enderror" placeholder="Provinsi"
                                value="{{ old('provinsi') }}">
                            @error('provinsi')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <label for="nik" class="form-label">NIK</label>
                            <input type="text" name="nik" class="form-control @error('nik') is-invalid @enderror"
                                placeholder="NIK" value="{{ old('nik') }}">
                            @error('nik')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col">
                            <label for="Email" class="form-label">E-mail</label>
                            <input type="email" name="email" class="form-control @error('email') is-invalid @enderror"
                                placeholder="E-mail" value="{{ old('email') }}">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <label for="telp" class="form-label">Telepon Rumah</label>
                            <input type="text" name="telp" class="form-control @error('telp') is-invalid @enderror"
                                placeholder="Telp" value="{{ old('telp') }}">
                            @error('telp')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col">
                            <label for="hp" class="form-label">Hp</label>
                            <input type="text" name="hp" class="form-control @error('hp') is-invalid @enderror"
                                placeholder="Hp" value="{{ old('hp') }}">
                            @error('hp')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <label for="tempatlahir" class="form-label">Tempat Lahir</label>
                            <input type="text" name="tempatlahir"
                                class="form-control @error('tempatlahir') is-invalid @enderror"
                                placeholder="Tempat Lahir" value="{{ old('tempatlahir') }}">
                            @error('tempatlahir')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col">
                            <label for="Tanggal Lahir" class="form-label">Tanggal Lahir</label>
                            <input type="date" name="tanggallahir"
                                class="form-control @error('tanggallahir') is-invalid @enderror"
                                value="{{ old('tanggallahir') }}">
                            @error('tanggallahir')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <label for="agama">Agama</label>
                            <select id="agama" name="agama" class="form-control @error('agama') is-invalid @enderror">
                                <option selected>Agama...</option>
                                <option value="islam">Islam</option>
                                <option value="kristen">Kristen</option>
                                <option value="katholik">Katholik</option>
                                <option value="budha">Budha</option>
                                <option value="hindu">Hindu</option>
                            </select>
                            @error('agama')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col">
                            <label for="jk">Jenis Kelamin</label>
                            <select id="jk" name="jk" class="form-control @error('jk') is-invalid @enderror">
                                <option selected>Jenis Kelamin...</option>
                                <option value="Laki-laki">Laki - Laki</option>
                                <option value="Perempuan">Perempuan</option>
                            </select>
                            @error('jk')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Status">Status Perkawinan</label>
                        <select id="Status" name="status_perkawinan"
                            class="form-control @error('status_perkawinanme') is-invalid @enderror">
                            <option selected>Status...</option>
                            <option value="belumkawin">Belum Kawin</option>
                            <option value="kawin">Kawin</option>
                            <option value="janda">Janda</option>
                            <option value="duda">Duda</option>
                        </select>
                        @error('status_perkawinanme')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <h5>Data Orang Tua</h5>
                    <div class="mb-3">
                        <label for="NamaAyah" class="form-label">Nama Ayah</label>
                        <input type="text" name="nameayah" class="form-control @error('nameayah') is-invalid @enderror"
                            id="NamaAyah" placeholder="Nama Ayah" value="{{ old('nameayah') }}">
                        @error('nameayah')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="row">
                        <div class="col">
                            <label for="alamat" class="form-label">Alamat</label>
                            <input type="text" name="alamatOrTua"
                                class="form-control @error('alamatOrTua') is-invalid @enderror"
                                placeholder="Alamat Orang Tua" value="{{ old('alamatOrTua') }}">
                            @error('alamatOrTua')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col">
                            <label for="kecamatan1" class="form-label">Kota / Kab</label>
                            <input type="text" name="kecamatanOrTua"
                                class="form-control @error('kecamatanOrTua') is-invalid @enderror"
                                placeholder="Kecamatan" value="{{ old('kecamatanOrTua') }}">
                            @error('kecamatanOrTua')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label for="kodepos" class="form-label">Kode Pos</label>
                            <input type="text" name="KodePosOrTua"
                                class="form-control @error('KodePosOrTua') is-invalid @enderror" placeholder="KodePos"
                                value="{{ old('KodePosOrTua') }}">
                            @error('KodePosOrTua')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col">
                            <label for="Provinsi" class="form-label">Provinsi</label>
                            <input type="text" name="ProvinsiOrTua"
                                class="form-control @error('ProvinsiOrTua') is-invalid @enderror" placeholder="Provinsi"
                                value="{{ old('ProvinsiOrTua') }}">
                            @error('ProvinsiOrTua')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label for="Telp" class="form-label">Telp Rumah</label>
                            <input type="text" name="TelpOrTua"
                                class="form-control @error('TelpOrTua') is-invalid @enderror" placeholder="Telp"
                                value="{{ old('TelpOrTua') }}">
                            @error('TelpOrTua')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col">
                            <label for="Hp" class="form-label">Hp Orang Tua</label>
                            <input type="text" name="hpOrTua"
                                class="form-control @error('hpOrTua') is-invalid @enderror" placeholder="Hp"
                                value="{{ old('hpOrTua') }}">
                            @error('hpOrTua')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label for="Pekerjaan" class="form-label">Pekerjaan Orang Tua</label>
                            <input type="text" name="pekerjaan"
                                class="form-control @error('pekerjaan') is-invalid @enderror" placeholder="Pekerjaan"
                                value="{{ old('pekerjaan') }}">
                            @error('pekerjaan')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col">
                            <label for="namaInstansi" class="form-label">Nama Instansi</label>
                            <input type="text" name="NamaInstansi"
                                class="form-control @error('NamaInstansi') is-invalid @enderror" placeholder="Instansi"
                                value="{{ old('NamaInstansi') }}">
                            @error('NamaInstansi')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Penghasilan">Penghasilan Orang Tua</label>
                        <select class="form-control @error('penghasilan') is-invalid @enderror" name="penghasilan"
                            id="Penghasilan">
                            <option value="<5juta">
                                < 5 Juta</option> <option value="5-7juta">5-7 Juta
                            </option>
                            <option value="7-10juta">7-10 Juta</option>
                            <option value=">10juta"> >10 Juta</option>
                        </select>
                        @error('penghasilan')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="row">
                        <div class="col">
                            <label for="Namaibu" class="form-label">Nama Ibu</label>
                            <input type="text" name="namaibu"
                                class="form-control @error('namaibu') is-invalid @enderror" placeholder="Nama Ibu"
                                value="{{ old('namaibu') }}">
                            @error('telnamaibu')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col">
                            <label for="Pekerjaanibu" class="form-label">Pekerjaan Ibu</label>
                            <input type="text" name="PekerjaanIbu"
                                class="form-control @error('PekerjaanIbu') is-invalid @enderror"
                                placeholder="Pekerjaan Ibu" value="{{ old('PekerjaanIbu') }}">
                            @error('PekerjaanIbu')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label for="mahasiswa">Sebagai Mahasiswa</label>
                            <select id="mahasiswa" name="mahasiswa"
                                class="form-control @error('mahasiswa') is-invalid @enderror">
                                <option selected>Mahasiswa...</option>
                                <option value="baru">Baru</option>
                                <option value="pindahan">Pindahan</option>
                                <option value="AlihJenjang">Alih Jenjang</option>
                                <option value="TugasBelajar">Tugas Belajar</option>
                            </select>
                            @error('mahasiswa')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <h5>Data Sekolah/PT Asal</h5>
                    <div class="row">
                        <div class="col">
                            <label for="smapt" class="form-label">SMA / PT</label>
                            <input type="text" name="smapt" class="form-control @error('smapt') is-invalid @enderror"
                                placeholder="SMA / PT" value="{{ old('smapt') }}">
                            @error('smapt')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col">
                            <label for="jurusan" class="form-label">jurusan</label>
                            <input type="text" name="jurusan"
                                class="form-control @error('jurusan') is-invalid @enderror" placeholder="jurusan"
                                value="{{ old('jurusan') }}">
                            @error('jurusan')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label for="alamatSekolahPt" class="form-label">Alamat</label>
                            <input type="text" name="alamatSekolahPt"
                                class="form-control @error('alamatSekolahPt') is-invalid @enderror" placeholder="Alamat"
                                value="{{ old('alamatSekolahPt') }}">
                            @error('alamatSekolahPt')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col">
                            <label for="ijazah" class="form-label">No. STTB / Ijazah</label>
                            <input type="text" name="noSttbIjazah"
                                class="form-control @error('noSttbIjazah') is-invalid @enderror"
                                placeholder="No. STTB / Ijazah" value="{{ old('noSttbIjazah') }}">
                            @error('noSttbIjazah')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label for="skhu" class="form-label">No. SKHU</label>
                            <input type="text" name="skhu" class="form-control @error('skhu') is-invalid @enderror"
                                placeholder="No. SKHU" value="{{ old('skhu') }}">
                            @error('skhu')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col">
                            <label for="tahunlulus" class="form-label">Tahun Lulus</label>
                            <input type="date" name="Tahun" class="form-control @error('Tahun') is-invalid @enderror"
                                value="{{ old('Tahun') }}">
                            @error('Tahun')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <label for="program">Program Studi</label>
                    <select id="program" name="prodi" class="form-control @error('prodi') is-invalid @enderror">
                        <option selected>Program Studi...</option>
                        <option value="D3-Kebidanan">D3 Kebidanan</option>
                        <option value="S1-Ilmu Gizi">S1 Ilmu Gizi</option>
                        <option value="S1-Keperawatan">S1 Keperawatan</option>
                        <option value="Profesi Ners">Profesi Ners</option>
                        <option value="S1-Management">S1 Management</option>
                        <option value="S1-Akuntansi">S1 Akuntansi</option>
                    </select>
                    @error('prodi')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror

                    <label for="Kelas">Kelas</label>
                    <select id="Kelas" name="kelas" class="form-control @error('kelas') is-invalid @enderror">
                        <option selected>Kelas...</option>
                        <option value="pagi">Pagi</option>
                        <option value="Sore">Sore</option>
                        <option value="Eksekutif">Eksekutif</option>
                        <option value="Alihjenjang">Alih Jenjang</option>
                    </select>

                    @error('kelas')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                    <button type="submit" class="btn btn-primary mt-1">Save</button>
                </form>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
</x-app-layout>